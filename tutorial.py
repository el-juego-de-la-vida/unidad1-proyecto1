#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import time
import os


def poblacion(matriz):

    contador = 0
    for i in range(18):
        for j in range(18):
            if matriz[i][j] == 'o':
                contador = contador + 1
    return contador


def imprimir(matriz):

    time.sleep(0.8)
    os.system('clear')
    print('JUEGO DE LA VIDA DE CONWAY\n')

    vivas = poblacion(matriz)
    print('Vivas:', vivas, '\tMuertas:', 324-vivas)

    for i in range(18):
        # Variable se actualiza en cada fila.
        casilla = ''
        for j in range(18):
            # Acumulación de las columnas de cada fila.
            casilla += matriz[i][j] + ' '
        print(casilla)
    print('\n')


def matriz_defecto():

    mdefecto = []
    for i in range(18):
        # Añadir listas para generar cada fila.
        mdefecto.append([])
        for j in range(18):
            # Añadir sublistas/columnas en cada fila.
            mdefecto[i].append('-')
    return mdefecto


def vecinos(matriz, x, y):

    vcontador = 0
    for i in range(-1, 2):
        if ((x + i < 0) or
           (x + i > 17)):
            continue
        for j in range(-1, 2):
            if ((y + j < 0) or
               (y + j > 17)):
                continue
            if ((x + i == x) and
               (y + j == y)):
                continue
            if (matriz[x + i][y + j] == 'o'):
                vcontador += 1
    return vcontador


# Recorrer posiciones para aplicar reglas del juego de la vida.
def recorrido(matriz):

    temporal = []
    temporal = matriz_defecto()

    for i in range(18):
        for j in range(18):
            # Variable recibe cantidad de vecinos según determinada célula.
            estado = vecinos(matriz, i, j)
            # Condiciones para célula viva.
            if matriz[i][j] == 'o':
                if estado == 2 or estado == 3:
                    temporal[i][j] = 'o'
            else:
                if estado == 3:
                    temporal[i][j] = 'o'

    imprimir(temporal)

    # Juego funciona mientras existe al menos una célula viva.
    while poblacion(temporal) > 0:
        # Actualización de matriz con nueva población.
        for i in range(18):
            for j in range(18):
                matriz[i][j] = temporal[i][j]
        recorrido(matriz)


if __name__ == '__main__':

    matriz = []
    contador = 0

    # Matriz original toma valor por defecto
    matriz = matriz_defecto()
    # Cantidad aleatoria de células vivas.
    cantidad_inicial = random.randint(60, 82)

    while contador != cantidad_inicial:
        x = random.randint(0, 17)
        y = random.randint(0, 17)
        matriz[x][y] = 'o'
        contador = poblacion(matriz)

    os.system('clear')
    imprimir(matriz)
    time.sleep(2.0)
    print("Que comience el juego...")
    time.sleep(1.0)
    os.system('clear')
    recorrido(matriz)
